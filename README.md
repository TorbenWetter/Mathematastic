# Mathematastic

A small program for 10th-graders to review and improve their mathematics skills after getting taught about the relevant topics.

The tasks are generated randomly and the user receives statistics on how he or she is doing.

## Getting Started

Use this guide to make a copy of the project on your computer for development and testing purposes.

### Prerequisites

You don't need special libraries to run this program, but you should have selected Tkinter in your Python installation.

If you haven't done that and you're using Linux, this can be done easily by using
```
sudo apt install python3-tk
```
That should also work with other package managers.

### Installing

All you have to do to install the program is downloading the source code, unzipping it and running
```
python3 main.py
```
in the console, located in the created directory.

## Built With

* [Tkinter](https://wiki.python.org/moin/TkInter) - The package to create graphical user interfaces and widgets with.

## Variables

The only global variables that are used in the program are:
* users_dir: The path of the user directory is stored in it to make it easier to access it in methods.
* root: The main Tkinter window, which is also used to create new subwindows.
* users: List of all users based on the user files. Changed when a user is created or deleted.
* selected_user: If one or more users exist, there will always be a selected user (the only user if only one user exists OR the user selected with the OptionMenu).
* happy_image/sad_image: The image object(s) to be displayed in the feedback window. Global, so that it only needs to be loaded once.

All other variables are attibutes or local variables (e.g. lists, Tkinter widgets, etc.).

## Operating Principle

When the program is executed, it checks whether users already exist. If more than one user exists, an OptionMenu is displayed, from which you can select the user you want to practice with. If at least one user exists, also a “Delete user” button is displayed. A “New user” button is included all the time. The program also displays the user’s statistics (percentage correct/wrong for each topic) below the greeting.

Regardless of whether a user has been created or not, a "Teaching" button is displayed, which opens a new window when pressed. In this window the user has the possibility to read something about all topics covered in this program (description and examples).

When the user clicks on the “Practise” button, the program generates questions for each topic and sorts them according to the percentage the user achieved in that topic. The questions are then selected one after the other and the user can answer them. Depending on the answer, the program gives a feedback about what was wrong with the answer or what was missing. Also it adds 1 to “right” or “wrong” in the user’s statistics file. Every question asked is removed from the question list and if there is no question left, new questions are generated. I try to ask questions in topics the user is not good enough in first, so the user gets trained in this topic.

## Running Tests

To test the program, create a new user (also try weird names), delete it, create new users, try to change the selected user, delete a few, and so on..

Then you can press the "Practise" button to test whether the tasks make sense, the results are calculated correctly or what happens when invalid input are entered.

### Purpose Of These Tests

It's important to perform these tests to find out how the program handles invalid entries or unexpected actions by the user.

For example wouldn't the program allow "Cristiano7" as a username, because usernames must not contain digits.

Also "~5" wouldn't be a valid answer to a question, because only digits (or commas to seperate multiple answers) are allowed in this Entry.

## Testing Table

Question                                | Answer        | Expected Result                                                                                       | Actual Result
--------------------------------------- | ------------- | ----------------------------------------------------------------------------------------------------- | -------------
Enter the new user's name               | Torben        | Enable the submit button, because the input is valid                                                  | as expected
Enter the new user's name               | DonaldDuck187 | Disable the submit button and display that this username is invalid                                   | as expected
Enter the new user's name               | *Enter press* | Nothing should happen                                                                                 | as expected
Write out the prime factorization of 18 | 2 and 3 and 4 | You can only use numbers (and commas) for your answer (invalid input)                                 | as expected
Write out the prime factorization of 18 | IaMaStRiNg123 | You can only use numbers (and commas) for your answer (invalid input)                                 | as expected
Write out the prime factorization of 18 | 0, 1, 2       | The numbers 0, 1 were wrong and the number 3 was missing. 2, 3, 3 would have been right (valid input) | as expected
Write out the prime factorization of 18 | 1, 2, 3       | The number 1 was wrong. 2, 3, 3 would have been right (valid input)                                   | as expected
Write out the prime factorization of 18 | 2, 3,4        | The number 4 was wrong. 2, 3, 3 would have been right (valid input)                                   | as expected
Write out the prime factorization of 18 | 2 3 4         | The number 4 was wrong. 2, 3, 3 would have been right (valid input)                                   | as expected
Write out the prime factorization of 18 | 2, 3          | Take a look at the amount of numbers you typed (valid input)                                          | as expected
Write out the prime factorization of 18 | 2,3,3         | Your answer was right (valid input)                                                                   | as expected
Write out the prime factorization of 18 | *Enter press* | Nothing should happen                                                                                 | as expected
