from random import randint, sample, choice


# generator to generate exercises and also calculating their results
# at the end of every method returning the question formatted with the values and the result (as a list)


def multiples_between(question):
    # select a random factor between 2 and 9
    factor = randint(2, 9)

    # select a random range of a size of 10 starting between 0 and 90 in steps of 10
    range_start = randint(0, 9) * 10
    range_end = range_start + 10

    # calculate the result by looping through the range and look for numbers whose remainder is 0 if divided by <factor>
    result = [i for i in range(range_start, range_end + 1) if i % factor == 0]

    return question.format(factor, range_start, range_end), result


def first_n_multiples(question):
    # generate a random amount between 3 and 5
    amount = randint(3, 5)

    # generate a random factor between 2 and 9
    factor = randint(2, 9)

    # calculate the result by multiplying the next <amount>+1 times the factor
    result = [(i + 1) * factor for i in range(amount)]

    return question.format(amount, factor), result


def least_common_multiple(question):
    # generate two not identical random factors between 6 and 10
    factor1, factor2 = sample(range(6, 10 + 1), 2)

    # calculate the result by getting the larger factor and increasing it by 1
    # until the remainder of the larger factor divided by both the first and the second factors is 0
    larger_factor = max(factor1, factor2)
    while True:
        if larger_factor % factor1 == 0 and larger_factor % factor2 == 0:
            return question.format(factor1, factor2), [larger_factor]
        larger_factor += 1


def first_n_common_multiples(question):
    # generate a random amount between 2 and 3
    amount = randint(2, 3)

    # generate two not identical random factors between 4 and 8
    factor1, factor2 = sample(range(4, 8 + 1), 2)

    # calculate the result by getting the larger factor and then saving all (<amount> of) numbers
    # whose remainder is 0 when dividing by both the first and the second factor
    larger_factor = max(factor1, factor2)
    result = []
    while len(result) < amount:
        if larger_factor % factor1 == 0 and larger_factor % factor2 == 0:
            result.append(larger_factor)
        larger_factor += 1

    return question.format(amount, factor1, factor2), result


def all_factors(question):
    # generate a random number by multiplying two random generated factors between 2 and 4 and between 5 and 7
    number = randint(2, 4) * randint(5, 7)

    # calculate and return the result by looping through all numbers between 1 and the <number>
    # and saving those whose remainder is 0 when you divide the <number> by them
    return question.format(number), [i for i in range(1, number + 1) if number % i == 0]


def rectangle_width(question):
    # generating several numbers with 4 or more factors (except factor 1)
    several_factor_numbers = []
    # loop through numbers between 12 and 50
    for number in range(12, 50 + 1):
        # getting all factors (remainder of number divided by them = 0) by looping through numbers
        # between 2 (to exclude the 1 as a factor) and the minimum of the number and 16 (so no factor > 16)
        factors = [factor for factor in range(2, min(number, 16 + 1)) if number % factor == 0]
        # append the number to the list if 4 or more factors were calculated
        if len(factors) >= 4:
            several_factor_numbers.append([factors, number])
    # select a random "several factor number" and extract their factors and the actual number into the area variable
    factors, area = choice(several_factor_numbers)
    # select two not identical random factors
    length1, length2 = sample(factors, 2)

    # calculate width1 for the question and width2 for the result by int-dividing the area by length1/length2
    return question.format(area // length1, length1, length2), [area // length2]


def primes_between(question):
    # select a random range of a size of 10 starting between 10 and 40 in steps of 10
    range_start = randint(1, 4) * 10
    range_end = range_start + 10

    # calculate the result by looping through the numbers in the range and looking for primes by looking if the
    # remainder of the number divided by all factors from 2 to the number itself (minus 1 to exclude it) is 0
    result = [i for i in range(range_start, range_end + 1) if all([i % factor for factor in range(2, i)])]

    return question.format(range_start, range_end), result


def first_n_primes(question):
    # generate a random amount between 2 and 4
    amount = randint(2, 4)

    # generate a random start between 10 and 40 in steps of 10
    start = randint(1, 4) * 10

    # calculate the result by appending 1 to number as long as <amount> primes bigger than the start are found
    # [primes are found the same way as in primes_between()]
    number = start
    result = []
    while len(result) < amount:
        if all([number % factor for factor in range(2, number)]):
            result.append(number)
        number += 1

    return question.format(amount, start), result


def only_prime_between(question):
    # generating several numbers being the only prime in a range of 5 between two multiples of 5
    several_only_primes = []
    # loop through numbers between 5 and 100 in steps of 5
    for range_start in range(5, 100 + 1, 5):
        # add 5 to the number to get the end of the range
        range_end = range_start + 5
        # calculating all primes in this range [like they are calculated in primes_between() or first_n_primes()]
        primes = [i for i in range(range_start, range_end + 1) if all([i % factor for factor in range(2, i)])]
        # append the range and the only prime to the list if there's only one prime in the range
        if len(primes) == 1:
            several_only_primes.append([range_start, range_end, primes[0]])
    # select a random "several only prime" range
    only_prime = choice(several_only_primes)

    # exstract the range for the question and the result from the prime object
    return question.format(*only_prime[:-1]), [only_prime[-1]]


def prime_factorization(question):
    # generate a random number by multiplying two random generated factors between 2 and 4 and between 5 and 7
    number = randint(2, 4) * randint(5, 7)

    # calculate the result
    result = []
    prime = 2
    count = number
    # as long as the square number of <prime> is smaller or equal than <count> (no more factors left if not)
    while prime ** 2 <= count:
        # and as long as <count> is bigger than 1
        while count > 1:
            # append <prime> to <result> and divide <count> by <prime>
            # as long as the remainder of <count> divided by <prime> is 0
            # then add 1 to the prime to do it again (if <count> is still bigger than 1)
            while count % prime == 0:
                result.append(prime)
                count /= prime
            prime += 1

    return question.format(number), result


def next_power(question):
    # generate a random start between 0 and 2
    start = randint(0, 2)

    # generate a random shift for all numbers between -1 and 1
    shift = randint(-1, 1)

    # calculate <sequence>, which includes 7 numbers (6 for the question and 1 for the result),
    # each number calculated by the shift added to the square of the current index added to the start
    sequence = [(start + i) ** 2 + shift for i in range(7)]

    # exstract the first 6 numbers for the question and the last number for the result from <sequence>
    return question.format(*sequence[:-1]), [sequence[-1]]


def evaluate_power(question):
    # generate a random number between 2 and 10
    number1 = randint(2, 10)

    # set to 2 if number1 is bigger than 5, else choose either 2 or 3 randomly
    number2 = randint(2, 3) if number1 <= 5 else 2

    # return the numbers inserted in the question, and return the calculated result (number1 to the power of number2)
    return question.format('{}{}'.format(number1, '²' if number2 == 2 else '³')), [number1 ** number2]


def squared_field_area(question):
    # generate a random side length between 2 and 12
    side = randint(2, 12)

    # return the side length inserted in the question,
    # and return the calculated area value (side length squared) as a result
    return question.format(side), [side ** 2]


def squared_field_side(question):
    # generate a random side length between 2 and 12
    side = randint(2, 12)

    # return the calculated area value (side length squared) inserted in the question,
    # and return the side length as a result
    return question.format(side ** 2), [side]


def cube_side(question):
    # generate a random side length between 1 and 5
    side = randint(1, 5)

    # return the calculated cube volume (side length cubed) inserted in the question,
    # and return the side length as a result
    return question.format(side ** 3), [side]


def evaluate_variables(question):
    # generate the variables a, b and c randomly (b and c between 1 and 20, a between 20 and 2x20)
    number_range = 20
    a, b, c = sample(range(1, number_range + 1), 3)
    a += number_range

    # return the generated values and the calculated result
    return question.format(a, b, c), [c - b + a]


def evaluate_expression1(question):
    # generate the variables a and b randomly (between 1 and 35)
    a, b = sample(range(1, 35 + 1), 2)

    # return the generated values and the calculated result
    # so the smaller number gets negative and the bigger number is added to it
    return question.format(min(a, b), max(a, b)), [-min(a, b) + max(a, b)]


def evaluate_expression2(question):
    # generate a random divisor (between 3 and 6) and a random factor (between 2 and 5)
    divisor = randint(3, 6)
    factor_min2 = randint(2, 5)
    # calculate the minuend and the dividend for the exercise (by multiplying the divisor times the factor)
    minuend = dividend = divisor * factor_min2

    # return the calculated result by substracting the factor from the dividend divided by the divisor
    # and substract the from the minuend
    return question.format(minuend, dividend, divisor, factor_min2), [minuend - int(dividend / divisor) - factor_min2]


def temperature_over_time(question):
    # generate a random start time (between 7pm and 10pm) and a random start temperature (between 21 and 26 degrees)
    start_pm = randint(7, 10)
    start_temp = randint(21, 26)

    # choose whether the time loss per hour is first 1 degree (and then 2) or first 2 degrees (and then 1)
    temp_loss1, temp_loss2 = sample(range(1, 2 + 1), 2)

    # calculate the time difference between the generated pm time and midnight
    time_diff1 = 12 - start_pm
    # calculate a random end time (between 1am and 4am) -> that's also the second time difference
    time_diff2 = end_am = randint(1, 4)

    # calculate the result by substracting the lost degrees from the generated start time to midnight to the end time
    result = start_temp - (temp_loss1 * time_diff1) - (temp_loss2 * time_diff2)
    return question.format(start_pm, start_temp, temp_loss1, time_diff1, temp_loss2, end_am, end_am), [result]


def thought_of_number(question):
    # generate a random number as the answer and a random number to add to the double of the answer (between 3 and 9)
    number, add = sample(range(3, 9 + 1), 2)

    # calculate the result which the user sees in the question
    result = number * 2 + add

    # choose a random name for the question
    name = choice(['Oliver', 'Jack', 'William', 'James', 'Benjamin', 'Olivia', 'Charlotte', 'Sophie', 'Emily', 'Ella'])

    # return the values for the question and the number the person thought of
    return question.format(name, add, result, name), [number]
