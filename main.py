"""
Mathematastic is a program for Year 10s to practise and improve their Mathematics.
After the user typed in his or her name, he can click on Practise to start.
The program will generate questions with random numbers (and so results) for different topics.
The worse the user is at a topic, the higher the priority that the user actually will practice this topic more.
So the program will prefer topics the user is bad at.
In the main menu the user also has an insight in his statistics (topic + percentage).
"""

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import re
import os
import sys
import json
import random
import base64
import generator


def get_valid_files(dir_path, valid_regex):
    files = []
    # get all files in the given directory..
    for file_name in os.listdir(dir_path):
        # .. which aren't directories and match the given userfile format
        if os.path.isfile(dir_path + file_name) and bool(re.match(valid_regex, file_name)):
            files.append(file_name)
    return files


def get_users():
    global users_dir
    # get valid user files
    user_files = get_valid_files(users_dir, '^[a-zA-Z]{2,16}.txt$')
    # for every user file, return its name without the .txt (=username)
    return [user_file.replace('.txt', '') for user_file in user_files]


def get_stats(username):
    # open the user file sitting in the users directory
    with open(users_dir + username + '.txt', 'r', encoding='utf8') as user_file:
        # decrypt the user file containing his stats
        decrypted_stats = crypt_stats(user_file.read(), username, False)
        try:
            # try to return the stats as a json object
            return json.loads(decrypted_stats)
        # if someone changed things in the user file, call the corrupt_user_file method and return an empty json object
        except json.decoder.JSONDecodeError:
            corrupt_user_file(username)
            return json.loads('{}')


def set_stats(username, stats):
    # open the user file sitting in the users directory
    with open(users_dir + username + '.txt', 'w+', encoding='utf8') as user_file:
        # encrypt the new stats and write them into the user file
        encrypted_stats = crypt_stats(json.dumps(stats), username, True)
        user_file.write(encrypted_stats)


def topic_percentage(username, stats, topic):
    # try to get the user's right and wrong values for the given topic
    try:
        # get the values and return the right-percentage (if no answers of that topic were answered, return 0)
        right = int(stats[topic]['right'])
        wrong = int(stats[topic]['wrong'])
        return int(float(right) / float(right + wrong) * 100) if right + wrong != 0 else 0
    # if the topic doesn't exist in the user file yet, add it with wrong and right equal to 0 and update the stats
    except KeyError:
        stats[topic] = json.loads('{"wrong": 0, "right": 0}')
        set_stats(username, stats)
        return 0


def list_string(l):
    # convert all int values to strings
    str_l = [str(value) for value in l]
    # return an empty string if no values are given,
    # the first value if only one value is given,
    # or all values, seperated by commas (between last two values " and "), if more than one value is given
    return '' if len(str_l) == 0 else str_l[0] if len(str_l) == 1 else ', '.join(str_l[:-1]) + ' and ' + str_l[-1]


def crypt_stats(stats_s, username, encrypt):
    # if the stats shall be encrypted, 64encode the stats appended to the username and decode the bytes to utf-8
    if encrypt:
        return base64.urlsafe_b64encode(username.encode() + stats_s.encode()).decode()
    else:
        # else, try to decode the stats from the file, remove the username and decode the bytes to utf-8
        try:
            return base64.urlsafe_b64decode(stats_s.encode())[len(username):].decode()
        # if the file was edited and can't be decoded, call the corrupt_user_file method and return an empty json string
        except UnicodeDecodeError:
            corrupt_user_file(username)
            return '{}'


def get_feedback_msg(typed_answer_list, correct_answer_list):
    # get the wrong answers by looking for answer that aren't included in the correct answers
    wrong = [answer for answer in typed_answer_list if answer not in correct_answer_list]
    # get the missing answers by looking for correct answers that are missing in the user's answers
    missing = [correct for correct in correct_answer_list if correct not in typed_answer_list]

    # build a message to show the user what exactly was wrong
    msg = 'You '

    # if answers were missing, list all of them, and then append an " and " if there were also wrong answers
    if len(missing) > 0:
        msg += 'forgot the number{} {}{}'.format('s' if len(missing) > 1 else '', list_string(missing),
                                                 ' and ' if len(wrong) > 0 else '.')

    # if answers were wrong, list all of them (also formatted with the list_string method)
    if len(wrong) > 0:
        msg += 'typed the number{} {} wrong.'.format('s' if len(wrong) > 1 else '', list_string(wrong))

    # if no answers were wrong or missing, but the answer itself is still wrong,
    # the amount of numbers in the answer must have been wrong. So add a message speaking of this
    if len(missing) == 0 and len(wrong) == 0:
        msg += 'should take a look at the amount of numbers you typed.'

    # return the built message
    return msg


def delete_user_file(username):
    # remove the user file sitting in the users directory
    os.remove(users_dir + username + '.txt')


def corrupt_user_file(username):
    # if the user file is corrupt, because it was edited,
    # close the window(s)
    global root
    root.destroy()

    # delete the corrupt user file
    delete_user_file(username)

    # show a message box to the user which explains what happened
    messagebox.showerror('ERROR', 'The user file is corrupt and had to be removed. Please restart the program.')

    # stop the program
    sys.exit()


class Welcome:
    def __init__(self, master):
        # use global users and selected_user variables
        global users, selected_user
        self.master = master

        # create a Frame in the window
        self.frame = ttk.Frame(self.master)

        user_amount = len(users)

        # create a label which welcomes the user (if no user exists it's just <welcome>)
        welcome = 'Welcome in Mathematastic'
        welcome_msg = welcome + '{}!'.format(', ' + selected_user.get() if user_amount > 0 else '')
        self.welcome_label = ttk.Label(self.frame, text=welcome_msg)
        # trace the selected_user variable to change user data when a new user gets selected
        selected_user.trace('w', lambda *args: self.change_user(welcome))
        self.welcome_label.pack()

        # create a LabelFrame to display the user's stats
        self.stats_area = ttk.LabelFrame(self.frame, text='Statistics')
        self.stats_var = tk.StringVar()
        self.stats_list = ttk.Label(self.stats_area, textvariable=self.stats_var)
        self.stats_list.pack()

        # if min. 1 user exists, update and show the stats LabelFrame
        if user_amount > 0:
            self.update_stats()
            self.stats_area.pack()

        # create and add a button, so a 'new user' window gets created on press
        new_user_button = ttk.Button(self.frame, text='New user', command=self.new_user_window)
        new_user_button.pack()

        # create a Teaching button, so the Teaching window gets created and opened on press
        teaching_button = ttk.Button(self.frame, text='Teaching', command=self.teaching_window)

        # create a 'delete user' and a 'practise' button which also execute their relating methods on press
        self.delete_user_button = ttk.Button(self.frame, text='Delete user', command=self.delete_user)
        self.practise_button = ttk.Button(self.frame, text='Practise',
                                          command=lambda: self.practise_window())

        # if min. 1 user exists, pack (show) the 'delete user' button
        if user_amount > 0:
            self.delete_user_button.pack()

        # pack the teaching button
        teaching_button.pack()

        # if min. 1 user exists, pack (show) the 'practise' button
        if user_amount > 0:
            self.practise_button.pack()

        # if more than 1 user exists, create an OptionMenu to show all users that can be selected
        if user_amount > 1:
            self.user_menu = ttk.OptionMenu(self.frame, selected_user, selected_user.get(), *users)
            self.user_menu.pack()

        # pack the frame into the window
        self.frame.pack()

    def change_user(self, welcome):
        # update the welcome label with the new username
        self.welcome_label.configure(text=welcome + '{}!'.format(', ' + selected_user.get() if len(users) > 0 else ''))
        # if there's min. 1 user now, update the stats and pack the stats LabelFrame
        if len(users) > 0:
            self.update_stats()
            self.stats_area.pack()
        # else, unpack the stats LabelFrame
        else:
            self.stats_area.pack_forget()

    def update_stats(self):
        # get the new username
        username = selected_user.get()

        # load the exercises file
        exercises = json.load(open('exercises.json', 'r', encoding='utf8'))
        # get the new stats for the user
        stats = get_stats(username)
        # add a new line for each topic in the exercises file which shows the topic name and the user's percentage
        stats_s = '\n'.join(
            '{}: {}%'.format(topic['name'], topic_percentage(username, stats, topic['name'])) for topic in exercises)

        # set this stats string to the stats StringVar() to update the Label
        self.stats_var.set(stats_s)

    def new_user_window(self):
        # create the NewUser window and pass the buttons and the user menu to access them after a user is created
        NewUser(tk.Toplevel(self.master), self.delete_user_button, self.practise_button, self.update_user_menu)

    def teaching_window(self):
        # create the Teaching window
        Teaching(tk.Toplevel(self.master))

    def practise_window(self):
        # create the Practise window and pass the update_stats method, so it can be accessed during practising
        Practise(tk.Toplevel(self.master), self.update_stats)

    def update_user_menu(self, deleted):
        # access global variables users and selected_user
        global users, selected_user
        user_amount = len(users)
        # if the user menu was created already (means that the user amount has been larger than 1 once)
        if hasattr(self, 'user_menu'):
            # if there were to users, but one was deleted and now there's only one left
            if deleted and user_amount == 1:
                # unpack the user menu and return (updating finished)
                self.user_menu.pack_forget()
                return

            # else, unpack the user menu, create a new one (with the new users) and pack it again
            self.user_menu.pack_forget()
            self.user_menu = ttk.OptionMenu(self.frame, selected_user, selected_user.get(), *users)
            self.user_menu.pack()
        # if more than one users exists now, create a new user menu (with the new users) and pack it
        elif user_amount > 1:
            self.user_menu = ttk.OptionMenu(self.frame, selected_user, selected_user.get(), *users)
            self.user_menu.pack()

    def delete_user(self):
        # access global variables users_dir, users and selected_user
        global users_dir, users, selected_user

        # delete the user file
        delete_user_file(selected_user.get())

        # update the users (read users directory again)
        users = get_users()
        user_amount = len(users)
        # if there are no more users now
        if user_amount == 0:
            # set selected_user to an empty string to trigger an update for .trace()
            selected_user.set('')
            # unpack the 'delete user' and the 'practise' button
            self.delete_user_button.pack_forget()
            self.practise_button.pack_forget()
        # else if users still exist
        else:
            # set the selected_user variable to the first user in the list (can be the only user)
            selected_user.set(users[0])
            # update the user menu (deleted=True)
            self.update_user_menu(True)


class NewUser:
    def __init__(self, master, delete_user_button, practise_button, callback):
        global users

        # save the Toplevel window as a "master" object attribute and prevent resizing of it
        self.master = master
        self.master.resizable(width=False, height=False)

        # save arguments as object attributes
        self.delete_user_button = delete_user_button
        self.practise_button = practise_button
        # also the callback method that will be executed after a new user is created (update_user_menu)
        self.callback = callback

        # create a frame in a new window
        self.frame = ttk.Frame(self.master)

        # create and pack an error message Label to show whether something's wrong with the name input
        self.error_msg = tk.StringVar()
        self.error_label = tk.Label(self.frame, textvariable=self.error_msg, fg='red')
        self.error_label.pack()

        # create and pack a StringVar variable for the typed name and track it to update the error message
        self.typed_name = tk.StringVar()
        self.typed_name.trace('w', lambda *args: self.update_error_msg())

        # create an input for the name, add the StringVar to it, pack it and focus it, so the user can start typing now
        name_input = ttk.Entry(self.frame, textvariable=self.typed_name)
        name_input.pack()
        name_input.focus()

        # create and pack a 'create user' button and execute the method to create a new user on click
        # also, the button can only be clicked when the name in the Entry is valid
        self.create_user_button = ttk.Button(self.frame, text='Create', command=self.create_new_user)
        self.create_user_button.pack()

        # update the error message for the first time
        self.update_error_msg()

        # pack the frame to show all elements at the same time (wouldn't be a big time difference, but..)
        self.frame.pack()

    def create_new_user(self):
        # access global variables users_dir and users
        global users_dir, users

        # get the typed username
        username = self.typed_name.get()

        # load the exercises file as a json object
        exercises = json.load(open('exercises.json', 'r', encoding='utf8'))

        # create an empty json object for the stats
        stats = json.loads('{}')
        # for every topic in the exercise file, create a json object in the stats json object
        # which contains 0's for right and wrong (=initial stats)
        for topic in exercises:
            stats[topic['name']] = json.loads('{"right": 0, "wrong": 0}')
        # set these "zero-stats" to the new user's file
        set_stats(username, stats)

        # get the new users (now also with the new one) and find out how many users exist now
        users = get_users()
        user_amount = len(users)

        # set the new user to the selected one
        selected_user.set(username)

        # if the user amount was 0 and is 1 now
        if user_amount == 1:
            # add the 'delete user' and the 'practise' button
            self.delete_user_button.pack()
            self.practise_button.pack()
        # else if more than one users exist now
        elif user_amount > 1:
            # call the callback = update the user menu (deleted=False)
            self.callback(False)

        # close the NewUser window at the end
        self.master.destroy()

    def update_error_msg(self):
        # get the currently typed username
        username = self.typed_name.get()

        # check whether the typed name would be valid (between 2 and 16 letters)
        name_valid = bool(re.match('^[a-zA-Z]{2,16}$', username))

        # check whether the typed name is available (when the users list doesn't contain it) -> lowered comparison
        name_available = username.lower() not in [name.lower() for name in users]

        # if the Entry is empty, the error message says that a username should be typed in
        if username == '':
            self.error_msg.set('Please type in a username.')
        # else if the name isn't valid, the message says what a valid username looks like
        elif not name_valid:
            self.error_msg.set('The name must consist of 2-16 letters.')
        # else if the name isn't available, the message says that a user with that name already exists
        elif not name_available:
            self.error_msg.set('A user already exists with this name.')
        # else, the message says that this name would be valid and available
        else:
            self.error_msg.set('This name is valid.')

        # if the typed name would be valid and available
        success = name_valid and name_available
        # set the error Label's color to green (else red)
        self.error_label.configure(fg='green' if success else 'red')
        # and set the 'create user' button enabled (else disabled)
        self.create_user_button.config(state='normal' if success else 'disabled')


class Teaching:
    def __init__(self, master):
        # save the Toplevel window as a "master" object attribute and prevent it to be resized
        self.master = master
        self.master.resizable(width=False, height=False)

        # create a tkinter Notebook which functions as a menu with tabs
        tabs = ttk.Notebook(self.master)
        tabs.pack()

        # load the exercises file as a json object
        exercises = json.load(open('exercises.json', 'r', encoding='utf8'))

        # for every topic in the exercise file
        for topic in exercises:
            # create a new Frame which functions as a tab
            tab = ttk.Frame(tabs)

            # add a header Label to the Frame to display the topic name (and pack it)
            header = ttk.Label(tab, text=topic['name'], font=('Verdana 10 bold', 16))
            header.pack()

            # add the description text (as a Label) for the topic and pack it as well
            description = ttk.Label(tab, text=topic['description'], wraplength=450)
            description.pack()

            # add a header Label for the examples and pack this Label
            examples_header = ttk.Label(tab, text='Examples', font=('Verdana 10 bold', 14))
            examples_header.pack()

            # add the examples text (as a Label) and pack it
            examples = ttk.Label(tab, text=topic['examples'], wraplength=450)
            examples.pack()

            # add the Frame (with all the texts) to the Notebook tabs
            tabs.add(tab, text=topic['name'])


class Practise:
    def __init__(self, master, stats_callback):
        # save the Toplevel window as a "master" object attribute and prevent resizing of it
        self.master = master
        self.master.resizable(width=False, height=False)

        # save the callback method (executed after a question was answered [update_stats]) as an object attribute
        self.stats_callback = stats_callback

        # get the currently select user (that will practise)
        self.user = selected_user.get()

        # create a Frame which will contain the elements
        self.frame = ttk.Frame(self.master)

        # create a topic StringVar and a Label to show the topic (bold); pack the Label
        self.topic = tk.StringVar()
        topic_label = ttk.Label(self.frame, textvariable=self.topic, font='Verdana 10 bold')
        topic_label.pack()

        # create a question StringVar and a Label to show the question (max. 200 pixels per line)
        self.question = tk.StringVar()
        question_label = ttk.Label(self.frame, textvariable=self.question, wraplength=200)
        # pack the Label with a horizontal margin of 10px
        question_label.pack(padx=10)

        # create a StringVar to save what the user typed as an answer
        self.typed_answer = tk.StringVar()

        # trace the StringVar and enable/disable the submit button depending on whether the Entry is empty or not
        self.typed_answer.trace('w', lambda *args: self.submit_button.config(
            state='disabled' if self.typed_answer.get().strip() == '' else 'normal'))

        # create an Entry in which the answer can be entered, pack it and focus it, so the user can start typing now
        self.answer_entry = ttk.Entry(self.frame, textvariable=self.typed_answer)
        self.answer_entry.pack()
        self.answer_entry.focus()

        # create a submit button for sending the typed answer and pack it
        self.submit_button = ttk.Button(self.frame, text='Send', command=self.button_press)
        self.submit_button.pack()

        # open the exercises file and load it as a json object
        exercises_file = open('exercises.json', 'r', encoding='utf8')
        self.exercises = json.load(exercises_file)

        # create an empty answer list
        self.answer = []

        # at the first time new questions have to be loaded in the questions list created later
        self.get_new_questions()
        # and the first answer has to be picked from this list
        self.load_question()

        # to show all elements, the frame gets packed
        self.frame.pack()

    def get_new_questions(self):
        # get the user's current stats
        stats = get_stats(self.user)

        # get the topic names from the exercises file
        topic_names = [obj['name'] for obj in self.exercises]
        # sort the topic names by the percentage the user has in this topic
        topic_names_sort_by_perc = sorted(topic_names, key=lambda t: topic_percentage(self.user, stats, t))

        # create a questions list
        self.questions = []
        # for every (sorted) topic name
        for topic_name in topic_names_sort_by_perc:
            # get the question for the topic into a list
            topic_ques = next(
                (topic_obj['questions'] for topic_obj in self.exercises if topic_obj['name'] == topic_name), [])
            # shuffle the questions
            random.shuffle(topic_ques)
            # append the question and the topic name both in a tuple to the questions list
            for question in topic_ques:
                self.questions.append((question, topic_name))

    def load_question(self):
        # if no question is remaining in the questions list
        if len(self.questions) == 0:
            # get new questions (sorted by the percentage..)
            self.get_new_questions()

        # get the first tuple in the questions list and exstract the question and the topic name
        picked_question, topic_name = self.questions.pop(0)
        # set the topic name to the topic-attribute
        self.topic.set(topic_name)

        # python-evaluate the picked question list using the generator class and
        # set the question to the question-StringVar and the answer to the answer-attribute
        question, self.answer = eval('generator.{}(\"{}\")'.format(*picked_question))
        self.question.set(question)

    def button_press(self):
        # get the typed answer and strip it (remove spaces at beginning and end)
        typed_answer = self.typed_answer.get().strip()

        # sort the answer-list and use it as correct_answer_list
        correct_answer_list = sorted(self.answer)
        # create a typed_answer_list where the input is written if it's correct
        typed_answer_list = []

        # if the question's answer consists only of one number
        if len(correct_answer_list) < 2:
            # get the first (and only) number of the correct_answer_list and save it as correct_answer
            correct_answer = str(correct_answer_list[0])
            # check if the input is correct (only consists of one number and no other characters like commas)
            valid_input = re.match('^\d+$', typed_answer)
            # the answer is correct if it's a valid string and equals the correct_answer
            correct = correct_answer == typed_answer if valid_input else False
        # else if the question expects more than one answer
        else:
            # replace possible splitting characters by the user with commas
            for c in [' ', ';', '.', '|', '-']:
                typed_answer = typed_answer.replace(c, ',')
            # check if the input is valid by removing all commas and matching it with a numeric regexp
            valid_input = re.match('^\d+$', typed_answer.replace(',', ''))
            # set correct to the valid_input boolean value. If the input isn't valid, the answer also won't be
            correct = valid_input
            # if the input is valid
            if valid_input:
                # split the input string at every comma and remove empty strings from the resulting list (using filter)
                # then convert the iterable created by filter() to a list and loop through every value in this list
                # convert every value to an integer and then sort the list and save it as typed_answer_list
                typed_answer_list = sorted([int(value) for value in list(filter(None, typed_answer.split(',')))])
                # the typed answers are correct if the correct_answer_list equals the typed_answer_list
                correct = correct_answer_list == typed_answer_list
            # as correct_answer save all the values (after they are converted to a string again) joined with commas
            correct_answer = ', '.join([str(value) for value in correct_answer_list])

        # if the answer was correct, set the feedback message according to that
        if correct:
            message_s = 'Your answer was right!'
        # else if the answer wasn't correct
        else:
            # if the input wasn't valid, say in the message that the user typed in invalid characters
            if not valid_input:
                message_s = 'You typed in invalid characters. Please only use numbers{}!'.format(
                    ' (separated by commas)' if len(correct_answer_list) > 1 else '')
            # else if the question expected an answer consisting of only one number,
            # just say that the answer was wrong
            elif len(correct_answer_list) < 2:
                message_s = 'You typed the wrong answer.'
            # else, get an according feedback message to which numbers were wrong, missing or right
            else:
                message_s = get_feedback_msg(typed_answer_list, correct_answer_list)
            # append the right answer to the message at the end
            message_s += '\n\n{} would have been right.'.format(correct_answer)

        # create a new window to show the feedback on the answer in and set the title to either "Correct" or "Wrong"..
        self.answer_feedback = tk.Toplevel()
        self.answer_feedback.wm_title('Correct' if correct else 'Wrong')

        # create a Label to show a feedback image in
        # if the answer was correct, the happy image is shown, otherwise the sad one
        feedback_image = tk.Label(self.answer_feedback, image=happy_image if correct else sad_image)
        feedback_image.pack()

        # create a Message to print the feedback message and pack it
        message = tk.Message(self.answer_feedback, text=message_s)
        message.pack()

        # disable the answer Entry to prevent the user from changing the answer after pressing the submit button
        self.answer_entry.configure(state='disabled')

        if correct:
            # if the answer was correct, after 2 and a half seconds,
            # run the destroy_top method (which will reset everything to get ready for the next question)
            self.answer_feedback.after(2500, self.destroy_top)
        else:
            # if the answer was wrong, create an OK button and run the destroy_top method when it gets clicked; pack it
            ok_button = ttk.Button(self.answer_feedback, text='OK', command=self.destroy_top)
            ok_button.pack()

        # get the latest stats from the user
        stats = get_stats(self.user)

        # work out which value in the stats object has to be changed
        value_to_change = 'right' if correct else 'wrong'

        # get the old value, add one to it and set it to the new value
        old_value = stats[self.topic.get()][value_to_change]
        stats[self.topic.get()][value_to_change] = int(old_value) + 1

        # set the new user's stats and update the stats in the user menu
        set_stats(self.user, stats)
        self.stats_callback()

    def destroy_top(self):
        # close the feedback window
        self.answer_feedback.destroy()

        # enable and focus the answer Entry again, so the next answer can be typed in
        self.answer_entry.configure(state='normal')
        self.answer_entry.focus()

        # clear the Entry (removes the last answer)
        self.typed_answer.set('')

        # load a new question
        self.load_question()


# create users folder if it doesn't exist
users_dir = os.getcwd() + '/users/'
if not os.path.exists(users_dir):
    os.makedirs(users_dir)

# initialize tkinter, set the default window title to "Mathematastic" and prevent resizing of the window
root = tk.Tk()
root.wm_title('Mathematastic')
root.resizable(width=False, height=False)

# get the currently saved users and save the first user in the selected_user variable if min. 1 user exists
users = get_users()
selected_user = tk.StringVar()
if len(users) > 0:
    selected_user.set(users[0])

# load the two images that are shown in the feedback window (make them also 10 times smaller [from 640x640 to 64x64])
happy_image = tk.PhotoImage(file='images/happy.png').subsample(10)
sad_image = tk.PhotoImage(file='images/sad.png').subsample(10)

# create a Welcome object to open the welcome window
Welcome(root)
# "mainloop" the tkinter object
root.mainloop()
